import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ImageBackground,
  FlatList,
  TextInput,
  Keyboard,
  Animated
} from "react-native";
import SelfMessage from "../../components/selfMessage";
import RecievedMessage from "../../components/receivedMessage";
import moment from "moment";
import configFile from "../../config/config";

export default class MessageScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      filter: {},
      options: {
        limit: 20,
        skip: 0,
        sort: { createdOn: -1 }
      },
      message: "",
      messageList: []
    };
  }
  sendMessage = query => {
    if (query === "") {
      return;
    }
    let messageState = {
      query,
      answer: ""
    };
    // this.state.messageList.unshift(messageState);
    // this.setState({});
    let url = `${configFile.SERVER_URL}/queryChatbot`;
    fetch(url, {
      method: "post",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        query,
        token: "3agcnp5ufl8g",
        contactNumber: this.props.navigation.state.params.contact_number
      })
    })
      .then(res => res.json())
      .then(parsedData => {
        const messageList = [...this.state.messageList];
        messageState.answer = parsedData.locals.template;
        messageList.unshift(messageState);
        this.setState({ messageList });
      })
      .catch(err => {
        this.state.messageList.shift();
        console.log("errorrrrrr", err);
      });
  };
  componentDidMount = () => {
    const initialMessage = [];
    initialMessage.push({
      query: "",
      answer:
        "Hi, Insurance policy Bot here! You can ask me questions regarding your insurance policy and I'll do my best to answer. How may I help you ?"
    });
    this.setState({ messageList: initialMessage });
  };
  render() {
    return (
      <View style={[LOCAL_STYLES.chatContainer, { backgroundColor: "#fff" }]}>
        <FlatList
          inverted={true}
          onEndReached={() => {
            // this.getNewMessagesOnScroll();
          }}
          data={this.state.messageList}
          keyExtractor={(item, index) => {
            return "Message" + Math.random();
          }}
          renderItem={({ item, index }) => {
            return (
              <View>
                {item.query.length > 0 ? (
                  <SelfMessage
                    text={item.query}
                    date={moment(item.createdOn).fromNow()}
                    key={index}
                  />
                ) : null}
                <RecievedMessage
                  text={item.answer}
                  date={moment(item.createdOn).fromNow()}
                  key={index}
                />
              </View>
            );
          }}
        />
        <InputBox sendMessage={this.sendMessage} />
      </View>
    );
  }
}

class InputBox extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      query: "",
      keyboardHeight: new Animated.Value(0)
    };
    this.messageSent = false;
  }

  componentDidMount() {
    this.kbe1 = Keyboard.addListener("keyboardDidShow", this._keyboardWillShow);
    this.kbe2 = Keyboard.addListener("keyboardDidHide", this._keyboardWillHide);
  }

  _keyboardWillShow = event => {
    Animated.timing(this.state.keyboardHeight, {
      toValue: event.endCoordinates.height - 287,
      duration: 100
    }).start();
  };

  _keyboardWillHide = event => {
    Animated.timing(this.state.keyboardHeight, {
      toValue: 0,
      duration: 100
    }).start();
  };

  render() {
    let { query } = this.state;
    return (
      <View>
        <View style={LOCAL_STYLES.textFieldContainer}>
          <TextInput
            style={{
              height: 36,
              width: "91%",
              alignSelf: "center",
              backgroundColor: "white",
              paddingHorizontal: 15
            }}
            placeholder="Write message"
            placeholderTextColor="#DFDFDF"
            // multiline={true}
            value={query}
            underlineColorAndroid="transparent"
            onChangeText={query => this.setState({ query })}
            onSubmitEditing={() => {
              this.props.sendMessage(this.state.query);
              this.state.query = "";
            }}
          />
        </View>
        <Animated.View
          style={{
            height: this.state.keyboardHeight
          }}
        />
      </View>
    );
  }
}

const LOCAL_STYLES = StyleSheet.create({
  chatContainer: {
    flex: 1,
    height: 700
  },
  textFieldContainer: {
    height: 64,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F1F1F1",
    width: "100%"
  }
});
