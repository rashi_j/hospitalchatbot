import React, { Component } from "react";
import { Alert, Keyboard } from "react-native";
import strings from "../../config/en";
import { NavigationActions, StackActions } from "react-navigation";
export default class OTPBase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      OTP: ""
    };
  }
  showAlert = message => {
    Alert.alert("", message, [{ text: "OK", onPress: null }]);
  };
  validateOTP = () => {
    let OTP = this.state.OTP.trim();
    if (OTP.length == 0) {
      this.showAlert(strings.empty_OTP);

      return false;
    }
    if (OTP && this.state.OTP.trim() !== "1234") {
      this.showAlert(strings.valid_OTP);
      this.setState({ OTP: "" });
      return false;
    }
    return true;
  };
  performLogin = async () => {
    let { contact_number } = this.props.navigation.state.params;
    Keyboard.dismiss();
    if (this.validateOTP()) {
      const resetAction = StackActions.reset({
        index: 0,
        actions: [
          NavigationActions.navigate({
            routeName: "MessageScreen",
            params: { contact_number }
          })
        ]
      });

      this.props.navigation.dispatch(resetAction);
      // this.props.navigation.navigate("MessageScreen", { contact_number });
    }
  };

  onChangeText = (field, value) => {
    this.state[field] = value;
    this.setState({});
  };
}
