import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Dimensions
} from "react-native";
import Base from "./OTPbase";
import styles from "../../style";
import strings from "../../config/en";
import TextInput from "../../components/textInput";
import Button from "../../components/button";
export default class LoginScreen extends Base {
  render() {
    return (
      <KeyboardAvoidingView
        style={styles.container}
        keyboardShouldPersistTaps="always"
      >
        <ScrollView
          keyboardShouldPersistTaps={"always"}
          contentContainerStyle={{
            flex: 1,
            justifyContent: "space-evenly",
            flexDirection: "column"
          }}
        >
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "space-evenly"
            }}
          >
            <Text style={{ fontSize: 30, textAlign: "center" }}>
              Verify Your mobile number
            </Text>
            <View>
              <TextInput
                labelColor="black"
                value={this.state.OTP}
                label="OTP"
                maxLength={4}
                capitalize="none"
                keyboardType={"numeric"}
                placeholder="Enter OTP"
                onChangeText={text => {
                  this.onChangeText("OTP", text);
                }}
              />
              <Button
                style={{ marginTop: 20, alignSelf: "center" }}
                title={strings.login}
                loading={false}
                disableButton={false}
                onPress={this.performLogin}
              />
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
