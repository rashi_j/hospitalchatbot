import React, { Component } from "react";
import {
  View,
  Text,
  ScrollView,
  KeyboardAvoidingView,
  Dimensions
} from "react-native";
import Base from "./loginBase";
import TextInput from "../../components/textInput";
import strings from "../../config/en";
import styles from "../../style";
import Button from "../../components/button";
export default class LoginScreen extends Base {
  render() {
    return (
      <KeyboardAvoidingView
        style={styles.container}
        keyboardShouldPersistTaps="always"
      >
        <ScrollView
          keyboardShouldPersistTaps={"always"}
          contentContainerStyle={{
            flexDirection: "column",
            flex: 1,
            justifyContent: "space-evenly"
          }}
        >
          <View
            style={{
              flex: 1,
              alignItems: "center",
              justifyContent: "space-evenly"
            }}
          >
            <Text style={{ fontSize: 30 }}>Login</Text>
            <View>
              <TextInput
                labelColor="black"
                value={this.state.phone}
                label="Moble Number"
                maxLength={15}
                capitalize="none"
                keyboardType={"numeric"}
                placeholder="Please Enter Mobile number"
                onChangeText={text => {
                  this.onChangeText("phone", text);
                }}
              />
              <Button
                style={{ marginTop: 20, alignSelf: "center" }}
                title={strings.send_OTP}
                loading={false}
                disableButton={false}
                onPress={this.sendOTP}
              />
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
