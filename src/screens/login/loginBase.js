import React, { Component } from "react";
import { Alert, Keyboard } from "react-native";
import { callWebService } from "../../utilities/serverApi";
import strings from "../../config/en";
import configFile from "../../config/config";
import { isEmpty } from "lodash";
export default class LoginBase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      phone: ""
    };
  }
  showAlert = message => {
    Alert.alert("", message, [{ text: "OK", onPress: null }]);
  };
  validatePhone = () => {
    let phone = this.state.phone.trim();
    if (phone.length == 0) {
      this.showAlert(strings.empty_phone);
      return false;
    }
    if (phone.length < 10 || !phone.match(/^\d+$/)) {
      this.showAlert(strings.valid_phone);
      this.setState({ phone: "" });
      return false;
    }
    return true;
  };
  sendOTP = () => {
    Keyboard.dismiss();
    if (this.validatePhone()) {
      const options = {
        method: "get",
        headers: {
          "Content-Type": "application/json"
        }
      };
      let url = `${configFile.SERVER_URL}/mobilenumber?mobile_number=${
        this.state.phone
      }`;
      fetch(url, options)
        .then(res => res.json())
        .then(parsedResponse => {
          if (isEmpty(parsedResponse.policy)) {
            this.showAlert(strings.no_policy);
            return;
          }
          this.props.navigation.navigate("OTPScreen", {
            contact_number: parsedResponse.policy[0].contact_number
          });
        })
        .catch(err => {
          this.showAlert("There is some Error !!");
          console.log("error is", err);
        });
    }
  };

  onChangeText = (field, value) => {
    this.state[field] = value;
    this.setState({});
  };
}
