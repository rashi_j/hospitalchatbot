import { StyleSheet } from "react-native";
import { isIphoneX } from "./utilities/common";

export default StyleSheet.create({
  container: { flex: 1 },
  buttonStyle: {
    height: 45,
    width: 203,
    borderRadius: 10,
    backgroundColor: "#696969",
    alignItems: "center",
    justifyContent: "center"
  },
  errorMessageTextStyle: {
    color: "red",
    // fontFamily: "OpenSans-Light",
    fontSize: 12
    // lineHeight:815
  },
  errorMessageContainerStyle: {
    alignSelf: "flex-start",
    height: 18,
    paddingLeft: 10
  },
  registerlabel: {
    color: "black",
    alignSelf: "center",
    // fontFamily: "OpenSans",
    fontSize: 15,
    fontWeight: "500",
    marginVertical: 6
  },
  formInputText: {
    color: "#000000",
    fontSize: 16,
    fontFamily: "OpenSans",
    paddingTop: 7,
    paddingBottom: 7,
    paddingLeft: 14,
    paddingRight: 14
  },
  registerText: {
    fontSize: 16,
    // fontFamily: "OpenSans",
    // fontWeight: "300",
    color: "#646464",
    // marginTop: 7,
    alignSelf: "center"
  },
  formInputWrapper: {
    width: 263,
    height: 36,
    borderRadius: 10,
    backgroundColor: "#ffff",
    paddingLeft: 14
  },
  chatContainerStyle: {
    marginVertical: 10,
    marginRight: 20,
    marginLeft: 20,
    paddingHorizontal: 15,
    paddingTop: 15,
    paddingBottom: 10,
    borderRadius: 5,
    maxWidth: "75%"
  },
  chatTimeStyle: {
    alignSelf: "flex-end",
    fontSize: 10,
    color: "#7D7D7D",
    // fontFamily: "Roboto-Light",
    marginLeft: 30
  },
  chatTextStyle: {
    fontSize: 16,
    color: "black"
    // fontFamily: "Roboto-Light"
  }
});
