import React, { Component } from "react";
import { Text, View } from "react-native";
import styles from "./../style";
export default (ReceivedMessage = props => {
  return (
    <View
      style={{
        width: "100%",
        alignItems: "flex-start"
      }}
    >
      <View style={[styles.chatContainerStyle, { backgroundColor: "#EFEFEF" }]}>
        <Text style={styles.chatTextStyle}>{props.text}</Text>
        {/* <Text style={styles.chatTimeStyle}>{props.date}</Text> */}
      </View>
    </View>
  );
});
