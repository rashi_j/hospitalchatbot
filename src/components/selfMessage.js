import React, { Component } from "react";
import { Text, View } from "react-native";
import styles from "./../style";
let triangleStyle = {
  width: 0,
  height: 0,
  backgroundColor: "transparent",
  borderStyle: "solid",
  borderLeftWidth: 10,
  borderRightWidth: 10,
  borderBottomWidth: 30,
  borderLeftColor: "transparent",
  borderRightColor: "transparent",
  borderBottomColor: "#696969",
  transform: [{ rotate: "90deg" }]
};
let Triangle = style => {
  return <View style={[triangleStyle, style]} />;
};

export default (SelfMessage = props => {
  return (
    <View
      style={{
        width: "100%"
      }}
    >
      <View
        style={[
          styles.chatContainerStyle,
          { backgroundColor: "#696969", alignSelf: "flex-end" }
        ]}
      >
        <Text style={[styles.chatTextStyle, { color: "white" }]}>
          {props.text}
        </Text>
        {/* <Text style={styles.chatTimeStyle}>{props.date}</Text> */}
      </View>
      {Triangle({
        position: "absolute",
        right: 10,
        bottom: 20
      })}
    </View>
  );
});
