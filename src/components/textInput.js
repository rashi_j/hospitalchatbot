import React, { Fragment } from "react";
import { View, Text, TextInput } from "react-native";
import styles from "../style";

export default (InputField = props => {
  let {
    label,
    placeholder,
    value,
    onChangeText = () => {},
    errorField,
    labelColor = "black",
    capitalize = "sentences",
    secureText = false,
    keyboardType = "default",
    errorContainerStyle = {},
    editable = true,
    maxLength = 500,
    multiline = false
  } = props;
  return (
    <Fragment>
      <Text style={[styles.registerlabel, { color: labelColor }]}>{label}</Text>
      <View style={[styles.formInputWrapper, props.formInputWrapper]}>
        <TextInput
          keyboardType={keyboardType}
          multiline={multiline}
          editable={editable}
          value={value}
          maxLength={maxLength}
          style={styles.formInputText}
          placeholder={placeholder}
          placeholderTextColor="#9b9b9b"
          // underlineColorAndroid="#ffff"
          underlineColorAndroid={"transparent"}
          onChangeText={onChangeText}
          autoCapitalize={capitalize}
          secureTextEntry={secureText}
        />
      </View>

      {/* <View style={[styles.errorMessageContainerStyle, errorContainerStyle]}>
        <Text style={styles.errorMessageTextStyle}>{errorField}</Text>
      </View> */}
    </Fragment>
  );
});
