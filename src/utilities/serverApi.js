import axios from "axios";
import configFile from "../config/config";

const callWebService = options => {
  const axiosInstance = axios.create({
    baseURL: configFile.serverURL,
    withCredentials: true,
    timeout: 1000 * 10
  });
  return axiosInstance(options);
};

/**
 * options=
 * {
 *  method:"GET","POST","PUT","DELETE",
 * url:"/user?quer"
 * data:{
 *  content
 * }
 * }
 */

export { callWebService };
