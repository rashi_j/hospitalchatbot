import React, { Component } from "react";
import { StyleSheet, Text, View, YellowBox } from "react-native";
import { createStackNavigator } from "react-navigation";
import { LoginScreen } from "./src/screens/login";
import { OTPScreen } from "./src/screens/OTP";
import { MessageScreen } from "./src/screens/message";

YellowBox.ignoreWarnings([
  "Warning: isMounted(...) is deprecated",
  "Module RCTImageLoader"
]);

const Navigator = createStackNavigator(
  {
    LoginScreen: {
      screen: LoginScreen
    },
    OTPScreen: { screen: OTPScreen },
    MessageScreen: { screen: MessageScreen }
  },

  {
    headerMode: "none",
    initialRouteName: "LoginScreen"
  }
);
export default class App extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    return (
      <View style={styles.container}>
        <Navigator />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});
